#Has to be ran as Domain Admin
$OU = Get-ADOrganizationalUnit -SearchScope Subtree -SearchBase "OU=!!! TO BE DELETED !!!,OU=Needle Roller,DC=nrb,DC=inside" -Filter * -Properties distinguishedName, Name | select Name, distinguishedName | Sort-Object Name
foreach($ous in $OU){
    Set-ADOrganizationalUnit -Identity $ous.distinguishedName -ProtectedFromAccidentalDeletion $false
}
Remove-ADOrganizationalUnit -Identity <#Parent OU#> -Recursive