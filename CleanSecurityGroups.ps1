$SECGroup = Get-ADGroup -Filter * -Properties Name, samaccountname, DistinguishedName | Select Name, samaccountname, DistinguishedName | Sort-Object Name | where {$_.DistinguishedName -notlike "*CN=Builtin,DC=nrb,DC=inside*"}

#log file creation
$fileDate = (Get-Date -Format "MM-dd-yyyy").ToString()
$filePath = 'D:\Data\AD\ADCleanup'
$logFile = "$($filepath)\$($filedate)RemovedSecurityGroups_log.txt"
try{
New-Item -Path . -Name $filepath$fileDate"RemovedSecurityGroups_log.txt" -ErrorAction Stop
New-Item -Path $logFile -ErrorAction Stop
}
catch [System.IO.IOException]
{
Remove-Item $logFile
New-Item -Path $logFile
}

foreach($group in $SECGroup)
{
    try{
    $memberCount = Get-ADGroupMember -Identity $group.samaccountname | Measure-Object | select count
    }
    catch [Microsoft.ActiveDirectory.Management.ADException]{
        "GROUP: "+$group.Name
        Write-Host "Group is too large to examine. Moving to next object." -ForegroundColor Magenta
        "`r`n"
        continue
    }
    "GROUP: "+$group.Name
    "COUNT: "+$memberCount.Count
    if($memberCount.Count -lt 1) #CHANGE THIS TO 2 WHEN CHECKING GROUPS THAT IS 1 OR LESS
    {
        #Remove-ADGroup -Identity $group.Name
        Write-Host $group.Name"Group Removed" -ForegroundColor Cyan
        Remove-ADObject -Identity $group.DistinguishedName -Recursive -Confirm:$false
        Add-Content -Path $logfile -Value ("Deleted $($group.Name) ") -NoNewline
        Add-Content -Path $logfile -Value (Get-Date)
    }
    else
    {
        $i = get-adgroupmember -Identity $group.samaccountname | select samaccountname
        $c = $p = 0
        foreach($user in $i)
        {
            $tempPC = $temp = $null
            try{
                $temp = Get-ADUser -Identity $user.SamAccountName -Properties samaccountname, enabled, sAMAccountType, msExchRecipientTypeDetails | select SamAccountName, enabled, sAMAccountType, msExchRecipientTypeDetails -ErrorAction SilentlyContinue
            }
            catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]{
                #checks if the object is a computer instead
                try{    
                    $tempPC = Get-ADComputer -Identity $user.samaccountname -Properties samaccountname, enabled, sAMAccountType | select SamAccountName, enabled, sAMAccountType -ErrorAction SilentlyContinue
                }
                catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]{}
            }
            #checks if user is disabled > user account > user account that's not a room resource > and not a sharedMBX
            if($temp.enabled -eq $false -and $temp.sAMAccountType -eq "805306368" -and $temp.msExchRecipientTypeDetails -ne "8589934592" -and $temp.msExchRecipientTypeDetails -ne "34359738368")
            {
                $c++
            }elseif ($tempPC.enabled -eq $false -and $tempPC.sAMAccountType -eq "805306369") {
                $p++
            }
        }

        if($p -eq 0){ #checks if the group had computers/groups or users and will calculate accordingly
                if($c/$memberCount.Count*100 -eq 100){ #CHANGE BOTH IF STATEMENTS TO -GT 50 TO CHECK GROUP MEMBERSHIP W/HALF OF USERS DISABLED
               "Percentage: "+$c/$memberCount.Count*100
                Write-Host "All users are disabled in this group. Delete this group" -ForegroundColor Yellow
                Remove-ADObject -Identity $group.DistinguishedName -Recursive -Confirm:$false
                Add-Content -Path $logfile -Value ("Deleted $($group.Name) ") -NoNewline
                Add-Content -Path $logfile -Value (Get-Date)
            }
            else{
                "Percentage: "+$c/$memberCount.Count*100
                #Write-Host "Less than 50% of users are disabled. Keep group" -ForegroundColor Green
                Write-Host "At least one member is active. Keep Group." -ForegroundColor Green
            }
        }
        else{
            if($p/$memberCount.Count*100 -eq 100){
                "Percentage: " + $p/$memberCount.Count*100
                Write-Host "All objects are disabled in this group. Delete this group" -ForegroundColor Yellow
                Remove-ADObject -Identity $group.DistinguishedName -Recursive -Confirm:$false
                Add-Content -Path $logfile -Value ("Deleted $($group.Name) ") -NoNewline
                Add-Content -Path $logfile -Value (Get-Date)
            }
            else{
                "Percentage: " + $p/$memberCount.Count*100
                #Write-Host "Less than 50% of the objects are disabled. Keep group" -ForegroundColor Green
                Write-Host "At least one object is active. Keep Group." -ForegroundColor Green
            }
        }
    }
   "`r`n"
}