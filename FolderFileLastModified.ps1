$filePath = "\\wesvfp01\Public"
$date = (Get-Date).AddMonths(-24)
$data = Get-ChildItem -Path $filePath | select Name, LastAccessTime | Sort-Object LastAccessTime -Descending
#$c = 0
foreach($item in $data)
{
    if($item.LastAccessTime -lt $date) 
    {
        Write-Host $item.Name -ForegroundColor Green " " -NoNewline
        Write-Host $item.LastAccessTime -ForegroundColor Yellow
        #$c++
    }        
    else
    {}
}