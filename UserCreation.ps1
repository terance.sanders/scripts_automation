##### Excel Module needed. run as admin: Install-Module -Name ImportExcel #####
$SGSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "http://nrbvexch1601.nrb.inside/PowerShell/"
Import-PSSession $SGSession

[bool]$counter = 1
$rowCount = 4
$Location = Get-ChildItem -Path "./" -Include *.xlsx -Name
if($Location -gt 1)
{
    $i = 0
    Write-Host "The following Excel spreadsheets were found." -ForegroundColor Yellow
    foreach($file in $Location)
    {
        Write-Host '[-'$i'-]' $file
        $i++
    }
    Write-Host "Type 'C' to cancel" -ForegroundColor Yellow
    $i = Read-Host "`r`nPlease select the file you would like to read from"
    if($i -like 'c' -or $i -like 'C')
    {
        Get-PSSession | Remove-PSSession
        Exit-PSSession
    }
}

while ($counter -eq $true) 
{
    $Information = Import-Excel -Path $Location[$i] -StartColumn 1 -EndColumn 1 -StartRow $rowCount -EndRow $rowCount -NoHeader

    if($counter -eq $true -and $Information.P1)
    {
        $Information = Import-Excel -Path $Location[$i] -StartRow $rowCount -NoHeader -EndRow $rowCount
        $mgr = $Information.P10.Replace(" ", ".")+"@jtekt.com"

        $ManagerSAM = Get-ADUser -Filter { mail -eq $mgr } | Select-Object samAccountName

        ##### Parse DistinguishName to Variable *Stores each split in array* #####
        if($Information.P1 -like "MEXICO - JAMX U*")
        {
            $OU = "OU=JAMX Users,OU=JAMX,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside"
        }
        else 
        {
            $OU = $Information.P1 -split " - ",2
        }

        $Country = ""
        if($Information.P20 -like "M*")
        {   $Country = "MX"}
        elseif ($Information.P20 -like "C*") 
        {   $Country = "CA"    }
        else 
        {   $Country = "US"    }

        ##### Forces first.last in case of user error #####
        $samAccountName = ($Information.P4+"."+$Information.P5).ToLower() -replace '\s+',''
        $UPN = $samAccountName+"@jtekt.com"

        (
        New-ADUser  -Name $Information.P7 -GivenName $Information.P4 -Surname $Information.P5 -SamAccountName $samAccountName -DisplayName $Information.P7 `
                    -Description $Information.P11 -OfficePhone $Information.P21 -StreetAddress $Information.P15 -City $Information.P17 `
                    -State $Information.P18 -PostalCode $Information.P19 -Country $Country -Title $Information.P11 `
                    -Department $Information.P13 -Company $Information.P12 -Manager $ManagerSAM -UserPrincipalName $UPN `
                    -Enabled $true -AccountPassword (ConvertTo-SecureString -AsPlainText "P@ssword123" -Force) -ChangePasswordAtLogon $true `
                    -Path $OU[1]
        )
        ################### Add User to Default Groups ######################
        Add-ADGroupMember -Identity JNA_O365_E3_Basic -Members $samAccountName
        Add-ADGroupMember -Identity JNA_O365_MFA -Members $samAccountName
        Add-ADGroupMember -Identity JNA_O365_SSPR -Members $samAccountName

        if($Information.P24 -like "Y*")
        {
            Add-ADGroupMember -Identity "Salaried Employees" -Members $samAccountName
        }
        else{}
        if($OU -eq "OU=PIE-Piedmont,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside")
        {
            Add-ADGroupMember -Identity "All JASC Team Members" -Members $samAccountName
        }
        else{}
        
        ################### Enable Remote Mailbox User ######################
        $user = $samAccountName
        $PrimarySMTP = $UPN
        $RemoteRouting = $user + "@jtekt.mail.onmicrosoft.com"
        $domainController = "nrbvdc19a.nrb.inside"

        Enable-RemoteMailbox -Identity $user -PrimarySmtpAddress $PrimarySMTP -RemoteRoutingAddress $PrimarySMTP -DomainController $domainController
        Set-RemoteMailbox -Identity $user -EmailAddresses @{add=$RemoteRouting} -RemoteRoutingAddress $RemoteRouting -DomainController $domainController

        $rowCount++     
    }
    else 
    {   
        $counter = 0   
        Get-PSSession | Remove-PSSession
    }
}
Set-Clipboard -Value "Username(s) will be: firstName.lastName `r`nEmail address will be: firstName.LastName@jtekt.com `r`nPassword is: P@ssword123 `r`nUser(s) will be prompted to change password after logging in."
Write-Host "The Following was copied to your clipboard and is ready to be pasted in the ticket:" -ForegroundColor Yellow
Write-Host "Username(s) will be: firstName.lastName `r`nEmail address will be: firstName.LastName@jtekt.com `r`nPassword is: P@ssword123 `r`nUser(s) will be prompted to change password after logging in."
Get-PSSession | Remove-PSSession