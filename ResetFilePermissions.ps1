#To Run code remotely: https://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/#:~:text=How%20to%20Run%20PowerShell%20Commands%20on%20Remote%20Computers,Command.%20...%205%20Start%20a%20Remote%20Session.%20
#Run command on remote machine *this example is SVPIEFP01*
Enter-PSSession -ComputerName SVPIEFP01 -Credential sanderst9

#OPTIONAL# Get ACL from desired Parent folder
$GrabbedAclfromParent = (Get-Acl -Path "D:\engdata\EV_Disconnect_Models\Mazda_Disconnect")
#file path
$file_ = "D:\engdata\EV_Disconnect_Models\Mazda_Disconnect\1. Master_Model"

Set-Acl -Path $file -AclObject $GrabbedAcl -WhatIf

takeown /f $file_ /r /a                                           #assigns local admin as folder owner
icacls.exe $file_ /inheritancelevel:e /t                          #enables inheritance so that it replicates properly to child-objects

foreach($file in Get-ChildItem -Path $file_ -Recurse | %{$_.FullName})
{
icacls.exe $file /Reset /t /c                                   #resets folder/file
icacls.exe $file /inheritancelevel:e                            #enables inheritance so that it replicates properly to child-objects
takeown /f $file /a                                             #sets owner to local server admin group
}

#Get the full path of the files and set owner and permissions as the above block
foreach($file in Get-ChildItem -Path "" -file -Recurse | %{$_.FullName})
{
icacls.exe $file /Reset /t /c                                   #resets file
takeown /f $file /a                                             #sets owner to local server admin group
icacls.exe $file /inheritancelevel:e                            #enables inheritance so that it replicates properly from Parent
}

Exit-PSSession