#documentation: http://www.rlmueller.net/AccountExpires.htm
#Expired-attribute:     132096240000000000
#Not-expired-attribute: 9223372036854775807
$NorthAmerica = "OU=JTEKT North America,DC=nrb,DC=inside"
$currentDate = Get-Date
$NADisabledUsersPath = "OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside"
$enabledExpired = Get-ADUser -Filter {Enabled -eq $true} -properties accountExpires, AccountExpirationDate -SearchBase $NorthAmerica | Select-Object Name, samAccountName, Enabled, accountExpires, AccountExpirationDate
New-Item -Path .\Active-Expired_Exported_Groups.txt
foreach($user in $enabledExpired)
{
    if($user.accountExpires -like "13*" -and $user.AccountExpirationDate -lt $currentDate)
    {
        #Confirm
        Write-Host $user.samAccountName "removed"
        Add-Content -Path .\Active-Expired_Exported_Groups.txt -Value $user.samAccountName

        <#remove from groups
        $ADgroups = Get-ADPrincipalGroupMembership -Identity $user.samAccountName | Where-Object {$_.Name -ne "Domain Users"}
        Remove-ADPrincipalGroupMembership -Identity $user.samAccountName -MemberOf $ADgroups -Confirm: $false

        #disable user
        Disable-ADAccount -Identity $user.samAccountName

        #move to NA Disabled OU
        Move-ADObject -Identity $user.DistinguishedName -TargetPath $NADisabledUsersPath#>
    }
    else{}
}