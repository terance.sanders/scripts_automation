﻿######################################################
#                                                    #
# Script to move objects from OUs to NA Disabled     #
#                                                    #
######################################################

#Log file creation
$fileDate = (Get-Date -Format "MM-dd-yyyy").ToString()
$filePath = 'D:\Data\AD\ADCleanup'
$logFile = "$($filepath)\$($filedate)_logComputerDisable.txt"
try{
New-Item -Path 'D:\Data\AD\ADCleanup' -Name $filepath$fileDate"_logComputerDisable.txt" -ErrorAction Stop
New-Item -Path $logFile -ErrorAction Stop
}
catch [System.IO.IOException]
{
Remove-Item $logFile
New-Item -Path $logFile
}
#Cut-off date 
$Date = (Get-Date).AddMonths(-3)

$DisabledComputerPath = "OU=NA Disabled Computers,OU=JTEKT North America,DC=nrb,DC=inside"
#$predeletePath = "OU=TestPredelete,OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside" FOR TESTING

#North America OUs[21] *Excludes Halifax and Weslake*
<#$OUs = @( 'OU=JAMX Computers,OU=JAMX,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=MOR Computers,OU=MOR-Morristown,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=PIE Computers,OU=PIE-Piedmont,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=VON Computers,OU=VON-Vonore,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=CHI Computers,OU=CHI-Chicago,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=GRE Computers,OU=GRE-Greenville,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=PLY Computers,OU=PLY-Plymouth,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=REM Computers,OU=REM-Remote,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=JTR Computers,OU=JTRNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=BED Computers,OU=BED-Bedford,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=CAI Computers,OU=CAI-Cairo,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=DAH Computers,OU=DAH-Dahlonega,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=KMX Computers,OU=KMX-Mexico,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=MEM Computers,OU=MEM-Memphis,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=OBG Computers,OU=OBG-Orangeburg,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=REN Computers,OU=REN-Reno,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=RLD Computers,OU=RLD-Richland,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=SYL Computers,OU=SYL-Sylvania,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=WAL Computers,OU=WAL-Walhalla,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=WAS Computers,OU=WAS-Washington,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=KJM Computers,OU=KJM-Mexico,OU=JTEKT North America,DC=nrb,DC=inside')#>
$OUs = "OU=GRE Computers,OU=GRE-Greenville,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside"
foreach($ou in $OUs)
{
    #Grabs users from OU and applies filters
    $AllDisabledComputers = Get-ADComputer -Filter {Enabled -eq $false} -SearchBase $ou -properties whenChanged, DistinguishedName | Select-Object DNSHostName, samAccountName, Enabled, DistinguishedName

    foreach($computer in $AllDisabledComputers)
    {      
        Add-Content -Path $logFile -Value ("Moved $($computer.samAccountName) ") -NoNewline
        Add-Content -Path $logFile -Value (Get-Date)
        Write-Host $computer.samAccountName
        #Move-ADObject -Identity $computer.DistinguishedName -TargetPath $DisabledComputerPath
        #Move-ADObject -Identity $computer.DistinguishedName -TargetPath $predeletePath FOR TESTING
    }
}
######################################################
#                                                    #
# Script to delete objects in NA Disabled/Pre-Delete #
#                                                    #
######################################################
$NAdisabledComputers = Get-ADComputer -Filter {Enabled -eq $false} -SearchBase $DisabledComputerPath -properties whenChanged | Select-Object Name, samAccountName, Enabled, whenChanged, distinguishedName
foreach($object in $NAdisabledComputers)
    {      
        if($object.whenChanged -lt $Date)
        {
            Add-Content -Path $logFile -Value ("Deleted $($object.samAccountName) ") -NoNewline
            Add-Content -Path $logFile -Value (Get-Date)
            $object.samAccountName
            #Remove-ADComputer -Identity $object.samAccountName -Confirm:$false
            #Remove-ADObject -Identity $object.distinguishedName -Recursive -Confirm:$false
        }
        else
        {
            #Do Nothing with account(s)
        }
    }