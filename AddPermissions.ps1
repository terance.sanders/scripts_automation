﻿$path = "E:\shares\data\Technology\Materials\JOBDATABASE"

$NewAcl = Get-Acl -Path $path
# Set properties
$username = "nrb\Qzhou"
#FullControl, Modify, Read
$fileSystemRights = "Modify"
$inheritance = 'ContainerInherit,ObjectInherit'
$progation = "None"
$type = "Allow"
# Create new rule
$fileSystemAccessRuleArgumentList = $username, $fileSystemRights, $inheritance, $progation, $type 
$fileSystemAccessRule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $fileSystemAccessRuleArgumentList
# Apply new rule
$NewAcl.SetAccessRule($fileSystemAccessRule)
Set-Acl -Path $path -AclObject $NewAcl