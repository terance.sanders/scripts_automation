﻿########## Additional Code: Output to file then read from the outputted file (for portability) ##########

#Finds any drives that has been mapped on current user machine
[string[]]$Drives = Get-ChildItem -Path Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\ | Select-Object Name | Where-Object {$_.Name -like "*##*"}

#truncates the return of Drives
$MappedDrives = $Drives | ForEach-Object{ $PSItem.Replace('#' , '\').Trim("@","{","}").Substring(87).ToString()}

if( Test-Path .\FoundDrives.txt)
{
    Write-Host "Text file found and is ready to read."
}
else {
    try
{
    $MappedDrives | Out-File -FilePath "FoundDrives.txt" -Force
    Write-Host "File for previous mapped drives has been created in current location.`r`nName: FoundDrives.txt" -ForegroundColor Yellow
}
catch [System.IO.IOException]
{
    Remove-Item "FoundDrives.txt"
    New-Item -Path . -Name "FoundDrives.txt"
}
}


$answer = Read-Host "Begin mapping process? (Y/N)" 

if($answer -ne "Y")
{
    exit
}
else 
{
    #Continue
    "`r`n"
}

################## Begin mapping process #######################
[bool]$pass = 0
[int32]$attempt = 0 
#Grabs user's credentials needed for New-PSDrive cmdlet
while($pass -eq $false)
{
    if($attempt -eq 4)
    {
        Write-Host "You are on your last attempt before getting locked out of your account." -ForegroundColor Yellow
    }

    $pass = 1
    $userID = $env:USERNAME
    $cred = Get-Credential -Credential nrb.inside\$userID -Verbose
    #A simple try-catch to test credentials entered
    try
    {
        Start-Process -FilePath cmd.exe /c -Credential $cred -ErrorAction Stop
    }
    Catch [System.InvalidOperationException]
    {
        $pass = 0
        $attempt++
        Write-Host "Failed Authentication. Please enter correct password" -foregroundcolor yellow
        Write-Host $attempt"/5"
    }
    if($attempt -eq 5)
    {
        Write-Host "You have been locked out of your account. Please contact your System Administators to unlock account." -foregroundcolor yellow
        Read-Host "Press enter to exit"
        exit
    }
}

Write-Host "`r`n"$Drives.Count"Mapped drives were found, Enter a letter that you want for each drive. Type: > or >> to skip drive`r`n"

[bool]$pass = 0
while($pass -eq 0)
{
    try {
        Get-Content "FoundDrives.txt"
        $pass = 1
    }
    catch [System.Management.Automation.ItemNotFoundException]{
        Write-Host "File does not exist in current path"
        $pass = 0
    }
}

foreach($line in Get-Content ".\FoundDrives.txt")
{
    $pass = 0
    while( $pass -eq 0)
    {
        $pass = 1
        #1 Ask for preferred letter for each mapping
        $letter = Read-Host "`r`nEnter a letter to set for" $line
        #existing mapped characters
        [char[]]$currentLetters = Get-PSDrive | Where-Object {$_.Provider -match "FileSystem" } | ForEach-Object Name
        
        if($letter -eq ">>" -or $letter -eq ">")
        {
            Write-Host "Path Skipped" -ForegroundColor Yellow
            $pass = 1
        }
        else
        {
            if($letter.Length -gt 1 -and $letter -ne ">>")
            {
                Write-Host "Drive letter must be ONE letter. `r`n" -ForegroundColor Yellow
                $pass = 0
            }
        }

        if($letter | Where-Object {$currentLetters -contains $letter})
        {
            Write-Host "Letter in use. The following letters cannot be used. `r`n"$currentLetters -ForegroundColor Yellow
            $pass = 0
        }

        if ($letter -match '[^a-zA-Z]' -and $letter -ne ">>" -and $letter -ne ">") 
        {
            Write-Host "Drive letter cannot contain numbers or special characters. `r`n" -ForegroundColor yellow
            $pass = 0
        }

        try
        {
            if($pass -eq 1 -and $letter -ne ">>" -and $letter -ne ">")
            {
                
                    New-PSDrive -Name ($letter) -Root $line -PSProvider "FileSystem" -Credential $cred -Persist -Scope Global -ErrorAction Stop
                    "`r`n"
                
            }
        }
        Catch [System.ComponentModel.Win32Exception]{
            Write-Host "This path is currently mapped. Proceeding to next drive... `r`n" -ForegroundColor Yellow
            $pass = 1
        }
    }
}
Read-Host "`r`nAll drives have been mapped. Press enter to finish"
exit