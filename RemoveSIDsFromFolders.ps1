$folderPath = "\\wesvfp01\Private\Payroll_Priv"
$SIDs = Get-Acl -Path $folderPath | select -ExpandProperty Access | select @{"N"="SID";"E"={$_.identityReference}} | Where-Object {$_.SID -like "*S-1-5*"}

foreach($entry in $SIDs)
{
$sid = $entry.SID.value
write-host "broken SID: "$sid
icacls $folderPath /inheritancelevel:d
Remove-NTFSAccess -AccessRights FullControl -Account $sid -Path $folderPath -AppliesTo ThisFolderSubfoldersAndFiles
}
