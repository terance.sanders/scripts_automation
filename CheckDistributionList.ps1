#$SGSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "http://nrbvexch1601.nrb.inside/PowerShell/"
#Import-PSSession $SGSession

#$DLGroup = Get-DistributionGroup -Filter * -ResultSize unlimited | Sort-Object Name

#log file creation
$fileDate = (Get-Date -Format "MM-dd-yyyy").ToString()
$filePath = '.\'
$logFile = "$($filepath)\$($filedate)RemovedDistroGroups_log.txt"
try{
New-Item -Path . -Name $filepath$fileDate"RemovedDistroGroups_log.txt" -ErrorAction Stop
New-Item -Path $logFile -ErrorAction Stop
}
catch [System.IO.IOException]
{
Remove-Item $logFile
New-Item -Path $logFile
}

foreach($group in $DLGroup)
{
    $memberCount = Get-DistributionGroupMember -Identity $group.samaccountname | Measure-Object | Select-Object Count
    "GROUP: "+$group.Name
    "COUNT: "+$memberCount.Count
    if($memberCount.Count -lt 1)
    {
        #Remove-DistributionGroup -Identity $group.Name
        Write-Host $group.Name"Group Removed" -ForegroundColor Cyan
        Add-Content -Path $logfile -Value ("Deleted $($group.Name) ") -NoNewline
        Add-Content -Path $logfile -Value (Get-Date)
    }
    else
    {
        $i = Get-DistributionGroupMember -Identity $group.samaccountname | select samaccountname
        $c = 0
        foreach($user in $i)
        {
            try{
            $temp = Get-ADUser -Identity $user.SamAccountName -Properties samaccountname, enabled, sAMAccountType | select SamAccountName, enabled, sAMAccountType -ErrorAction SilentlyContinue
            }
            catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]{
                "Member is a group object, not a user object. Cannot verify"
            }
            
            if($temp.enabled -eq $false -and $temp.sAMAccountType -eq "805306368")
            {
                $c++
            }
        }
            if($c/$memberCount.Count*100 -eq 100)
            {
                "Percentage: "+$c/$memberCount.Count*100
                Write-Host "All users are disabled in this distribution group. Delete this group." -ForegroundColor Yellow
                Add-Content -Path $logfile -Value ("Deleted $($group.Name) ") -NoNewline
                Add-Content -Path $logfile -Value (Get-Date)
            }
            else
            {
                "Percentage: "+$c/$memberCount.Count*100
                Write-Host "At least one member is active. Keep Group." -ForegroundColor Green
            }
    }
   "`r`n"
}