$Users = Import-Csv -path "koyo_ActiveDirectory_20230330001807275.csv" -Header EmpNumber, Status, Paytype, EmpId, FN, LN, MN, PFN, DisplayName, EmpEmail, Manager, MangerNumber, Position, paygroupxRef, RegionxRef, Dept, DeptxRef, Phone, Mobile, Address, City, State, Zip, CostCenter 
#Title Casing#
$textInfo = (Get-Culture).TextInfo

$StateAbbr = @{
    GA = "Georgia"
    IL = "Illinois"
    MI = "Michigan"
    NS = "Nova Scotia"
    NV = "Nevada"
    NY = "New York"
    ON = "Ontario"
    QC = "Quebec"
    SC = "South Carolina"
    TN = "Tenneessee"  
}
$SiteCode = @{
    "Arlington Heights" = "CHI"
    Bedford = "BED"
    Blythewood = "RLD"
    Cairo = "CAI"
    Dahlonega = "DAH"
    Greenville = "GRE"
    #typo - need HR fix#
    Halifx = "HAL"
    Memphis = "MEM"
    Mississauga = "KCI"
    MORRISTOWN = "MOR"
    Orangeburg = "OBG"
    PIEDMONT = "PIE"
    Plymouth = "PLY"
    Reno = "REN"
    Rochester = "JTR"
    Sylvania = "SYL"
    Telford = "WSH"
    VONORE = "VON"
    Walhalla = "WAL"
}
foreach($emp in $Users){
    $E1 = "" | Select first_name, last_name, user_name, password, email, status, addr1, addr2, country, state, city, zip, phone_no, manager_username, org_name, usertype, jobrole_code, jobtitle_code, empltype, employee_no , dept_code, is_manager, is_instructor, other_managers, other_organization, preferred_timezone, preferred_language, preferred_currency_code, hire_date

    $E1.first_name = $emp.FN
    $E1.last_name = $emp.LN
    # EmpID number#
    if($emp.EmpNumber.length -lt 7){
        while ($emp.EmpNumber.length -lt 7) {
            $emp.EmpNumber = "0"+$emp.EmpNumber
        }
        $E1.user_name = "`"" + $emp.EmpNumber + "`""
    }else {
        $E1.user_name = "`"" + $emp.EmpNumber + "`""
    }

    $E1.password = "`""+$emp.EmpNumber+"`""
    
    #email#
    if($emp.EmpEmail -notlike "*@jtekt.com"){
        $E1.email = $E1.user_name+"@jtekt.com"
    }else{
        $E1.email = $emp.EmpEmail
    }
    
    #Status#
    if ($emp.Status -eq "Inactive" -or $emp.Status -eq "Leave") {
        $E1.status = "Suspend"
    }else{
        $E1.status = "Active"
    }

    $E1.addr1 = $emp.Address
    $E1.addr2 = ""

    #Country#
    if($emp.State -eq "QC" -or $emp.State -eq "NS"){
        $E1.country = "Canada"
    }else{
        $E1.country = "United States"
    }
    #State/Province#
    $E1.state = $StateAbbr[$emp.State]

    #City to Titlecase#
    $E1.city = $textInfo.ToTitleCase($emp.City.ToLower())

    $E1.zip = $emp.Zip

    #Phone#
    if ($emp.Phone -ne "" -and $emp.Mobile -ne "") {
        $E1.phone_no = $emp.Phone
    }elseif($emp.Phone -eq "" -and $emp.Mobile -eq ""){
        $E1.phone_no = ""
    }elseif($emp.Phone -ne "" -and $emp.Mobile -eq ""){
        $E1.phone_no = $emp.Phone
    }else{
        $E1.phone_no = $emp.Mobile
    }

    # ManagerID number#
    if($emp.MangerNumber.length -lt 7){
        while ($emp.MangerNumber.length -lt 7) {
            $emp.MangerNumber = "0"+$emp.MangerNumber
        }
        $E1.manager_username = "`"" + $emp.MangerNumber + "`""
    }else {
        $E1.manager_username = "`"" + $emp.MangerNumber + "`""
    }

    $E1.org_name = $SiteCode[$emp.City]
    $E1.usertype = "Employee"
    $E1.jobrole_code = $emp.Position
    $E1.jobtitle_code = ""
    $E1.empltype = $emp.Paytype
    #reusing already formatted data#
    $E1.employee_no = $E1.user_name
    $E1.dept_code = $emp.Dept

    #is manager#
    if($Users.manager -eq $emp.DisplayName){
        $E1.is_manager = "Yes"
    }else{
        $E1.is_manager = "No"
    }

    $E1 | Export-Csv "Dayforce2E1.csv" -Append -NoTypeInformation
}   
