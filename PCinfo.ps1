get-computerinfo -property CsUserName,CsManufacturer, CsModel, CsName, OsArchitecture, OSLocalDateTime

$Status = Get-NetAdapter -InterfaceDescription "Cisco AnyConnect*" | Select-Object Status

if($Status -match "Up")
{
    Get-NetAdapter -InterfaceDescription "Cisco AnyConnect*" | Get-NetIPAddress | Select-Object IPAddress
}
else
{
    Write-Host "VPN connection not found. Please make sure you are connected in order to retrieve the correct IP Address." -ForegroundColor Yellow
}

Read-Host -Prompt "Press enter to exit"