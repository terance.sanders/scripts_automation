$Conf_members = Get-ADGroupMember -Identity "JNA_O365_E3_Conf" | Select-Object Name, samAccountName
foreach($member in $Conf_members)
{
    $member.Name
    Add-ADGroupMember -Identity "JNA_O365_E3_Basic" -Members $member.samAccountName
}
#Remove-ADGroup -Identity "JNA_O365_E3_Conf" -Confirm