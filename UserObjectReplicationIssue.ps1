$user =  "terance.sanders"
$DomainName = (Get-ADDomain).DNSRoot
$DCList = Get-ADDomainController -Filter * -Server $DomainName | Select-Object Hostname,Site,OperatingSystem | Where-Object {$_.Hostname -notlike "MUNVADC01.nrb.inside" -and
$_.Hostname -notlike "VZNVADC01.nrb.inside" -and $_.Hostname -notlike "KDVADC01.nrb.inside" -and $_.Hostname -notlike "IRY1DCNRB.nrb.inside" -and
$_.Hostname -notlike "IRYDC19A.nrb.inside" -and $_.Hostname -notlike "NJNRBVADC01.nrb.inside"}

foreach($site in $DCList)
{
    $i = $false
    $site.Hostname
    try
    {
    Get-ADUser -Identity $user -Server $site.Hostname | select Name, DistinguishedName
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        Write-Host $user" Not found on" $site.Hostname -ForegroundColor Red
        $i = $true
    }
    if($i -eq $false)
        {
            Write-Host $user" found on" $site.Hostname -ForegroundColor Green
        }
}