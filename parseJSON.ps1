$i = Get-Content -Raw -Path ".\E1_files\MOR---Safety---Ergonmics---General-Awareness--export.json" | ConvertFrom-Json
$dataTitle = $i.ordered_slides.draft_attributes | where{$_.title -ne $null -and $_.type -ne "slide"}
$estimated_time = $i.ordered_slides.estimated_time | where{$PSitem -ne 0} | measure-object -sum | select sum
if ($i.title -like "*Spanish*") {
    Write-Host "Language: Spanish" -ForegroundColor Yellow
}else {
    Write-Host "Language: English" -ForegroundColor Yellow
}
Write-Host "Course Title: "$i.title -ForegroundColor Yellow
Write-Host "Course Type: "$i.course_type -ForegroundColor Yellow
Write-Host "Description: "$i.description -ForegroundColor Yellow
Write-Host "Passing Grade: "$i.passing_threshold -ForegroundColor Yellow
Write-Host "Estimated completion time: "$estimated_time.Sum"minutes"-ForegroundColor Yellow "`r`n"

for ($c = 0; $c -lt $dataTitle.Count; $c++) {
        Write-Host $dataTitle[$c].type -ForegroundColor Magenta
        Write-Host $dataTitle[$c].title -ForegroundColor Cyan
        Write-Host $dataTitle[$c].answers.body -NoNewline $dataTitle[$c].answers.correct -Separator "`r`n" "`r`n"
    }