#Remove 8Accounts
$8accountUsers = Get-ADUser  -Filter * -SearchBase "OU=JTEKT North America,DC=nrb,DC=inside" -Properties samaccountname, DistinguishedName | Where-Object {$_.samaccountname -like "*8" -and $_.DistinguishedName -like "*8Account*"} | select samaccountname
foreach($user in $8accountUsers)
{
    #Remove-ADObject -Identity $user -Recursive
    #Disable-ADAccount -Identity $user.samAccountName
}
#Remove 8Account OU
$8ou = Get-ADOrganizationalUnit -filter {Name -like "*8Account*"} -SearchBase "OU=JTEKT North America,DC=nrb,DC=inside" -SearchScope Subtree -Properties * | Where-Object {$_.OU -like "*8Account*"} | Select-Object DistinguishedName
foreach($ou in $8ou)
{
    #Set-ADOrganizationalUnit -Identity $ou.DistinguishedName -ProtectedFromAccidentalDeletion $false
    #Remove-ADOrganizationalUnit -Identity $ou.DistinguishedName
}