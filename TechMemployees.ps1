$OU = "DC=nrb,DC=inside"

Get-ADUser -Filter * -SearchBase $OU -Properties Name, samAccountName, Enabled, Company, Description, DistinguishedName | Select-Object Name, samAccountName, Enabled, Company, Description, DistinguishedName | 
Where-Object {($_.Description -like "*TechM*") -or ($_.Description -like "*Tech M*") -or ($_.Company -like "*TechM*") -or ($_.Company -like "*Tech M*") -or
($_.Name -like "Techm")} | Export-Csv ".\TechMahindraJNAResources.csv" | Sort-Object Enabled -Descending