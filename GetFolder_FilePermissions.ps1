$folderPath = "\\grehfp01\Prototype"
$SIDs = Get-Acl -Path $folderPath | select -ExpandProperty Access | select @{"N"="SID";"E"={$_.identityReference}} @{"N"="Permissions";"E"={$_.AuditRuleType}} | Format-Table -HideTableHeaders

$i = foreach($user in $SIDs)
{
    Get-ADUser -Identity $user.SID.Value.Replace("NRB\","") -Properties * | select DisplayName | Format-Table -HideTableHeaders
}
#$i | Out-File payroll_root.txt