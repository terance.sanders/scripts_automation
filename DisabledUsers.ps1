﻿######################################################
#                                                    #
# Script to move objects from OUs to NA Disabled     #
#                                                    #
######################################################

#Log file creation
$fileDate = (Get-Date -Format "MM-dd-yyyy").ToString()
$filePath = 'D:\Data\AD\ADCleanup'
$logFile = "$($filepath)\$($filedate)_log.txt"
try{
New-Item -Path 'D:\Data\AD\ADCleanup' -Name $filepath$fileDate"_Log.txt" -ErrorAction Stop
New-Item -Path $logFile -ErrorAction Stop
}
catch [System.IO.IOException]
{
Remove-Item $logFile
New-Item -Path $logFile
}

#To Exclude SharedMailboxes from this move
$SharedMBXAttributes = 
#Shared Mailbox
"4",
#Remote Shared Mailbox
"34359738368"

#Cut-off date 
$Date = (Get-Date).AddMonths(-3)

$DisabledUsersPath = "OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside"
#$predeletePath = "OU=TestPredelete,OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside" FOR TESTING

#North America OUs[21] *Excludes Halifax and Weslake*
$OUs = @(   'OU=ENN Users,OU=ENN-Ennis,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside', 
            'OU=JAMX Users,OU=JAMX,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=MOR Users,OU=MOR-Morristown,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=PIE Users,OU=PIE-Piedmont,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=VON Users,OU=VON-Vonore,OU=JANA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=CHI Users,OU=CHI-Chicago,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=GRE Users,OU=GRE-Greenville,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=PLY Users,OU=PLY-Plymouth,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=REM Users,OU=REM-Remote,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=JTR Users,OU=JTRNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=BED Users,OU=BED-Bedford,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=CAI Users,OU=CAI-Cairo,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=DAH Users,OU=DAH-Dahlonega,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=KMX Users,OU=KMX-Mexico,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=MEM Users,OU=MEM-Memphis,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=OBG Users,OU=OBG-Orangeburg,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=REN Users,OU=REN-Reno,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=RLD Users,OU=RLD-Richland,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=SYL Users,OU=SYL-Sylvania,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=WAL Users,OU=WAL-Walhalla,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=WAS Users,OU=WAS-Washington,OU=KBNA,OU=JTEKT North America,DC=nrb,DC=inside',
            'OU=KJM Users,OU=KJM-Mexico,OU=JTEKT North America,DC=nrb,DC=inside')
#$OUs = "OU=Test_NA_Disabled_users,OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside" FOR TESTING

foreach($ou in $OUs)
{
    #Grabs users from OU and applies filters
    $AllDisabledUsers = Get-ADUser -Filter {Enabled -eq $false} -SearchBase $ou -properties whenChanged, DistinguishedName | Select-Object Name, samAccountName, Enabled, whenChanged, DistinguishedName | where {$_.msExchRecipientTypeDetails -ne 4 -or $_.msExchRecipientTypeDetails -ne 34359738368}

    foreach($user in $AllDisabledUsers)
    {      
        #Add-Content -Path $fileDate"_Log.txt" -Value ("Moved $($user.samAccountName) ") -NoNewline
        #Add-Content -Path $fileDate"_Log.txt" -Value (Get-Date)
        #Move-ADObject -Identity $user.DistinguishedName -TargetPath $DisabledUsersPath
        #Add-Content -Path $logFile -Value ("Moved $($user.samAccountName) ") -NoNewline
        #Add-Content -Path $logfile -Value (Get-Date)
        #Move-ADObject -Identity $user.DistinguishedName -TargetPath $DisabledUsersPath
        #Move-ADObject -Identity $user.DistinguishedName -TargetPath $predeletePath FOR TESTING
    }
}

######################################################
#                                                    #
# Script to delete objects in NA Disabled/Pre-Delete #
#                                                    #
######################################################
$NAdisabledUsers = Get-ADUser -Filter {Enabled -eq $false} -SearchBase $DisabledUsersPath -properties whenChanged | Where {$_.SamAccountName -notlike "svc*"} | Select-Object Name, samAccountName, Enabled, whenChanged, msExchRecipientTypeDetails, distinguishedName
#$NAdisabledUsers = Get-ADUser -Filter {Enabled -eq $false} -SearchBase $predeletePath -properties whenChanged | Select-Object Name, samAccountName, Enabled, whenChanged FOR TESTING


foreach($object in $NAdisabledUsers )
    {      
        if($object.whenChanged -lt $Date)
        {
            #Add-Content -Path $fileDate"_Log.txt" -Value ("Deleted $($object.samAccountName) ") -NoNewline
            #Add-Content -Path $fileDate"_Log.txt" -Value (Get-Date)
            Add-Content -Path $logfile -Value ("Deleted $($object.samAccountName) ") -NoNewline
            Add-Content -Path $logfile -Value (Get-Date)
            #Remove-ADObject -Identity $object.distinguishedName  -Recursive -Confirm:$false
        }
        else
        {
            #Do Nothing with account(s)
        }
    }


    $u = Read-Host "enter password"

    while ($u -eq $null) {
        Write-Host "not a valid password"
    }