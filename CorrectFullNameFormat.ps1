#Corrects FullName attribute in AD to standard format lastName, firstName
$Users = Get-ADUser -SearchBase "OU=GRE UserTest,OU=GRE-Greenville,OU=JNA,OU=JTEKT North America,DC=nrb,DC=inside" -Filter * -Properties givenname, sn, Name, DistinguishedName | Select-Object givenname, sn, Name, DistinguishedName
foreach ($user in $Users)
{
    $newName = $user.sn+", "+$user.givenname
    Rename-ADObject -Identity $user.DistinguishedName -NewName $newName
}