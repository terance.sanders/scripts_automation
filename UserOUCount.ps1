﻿$OUs = Get-ADOrganizationalUnit -Filter {(Name -like "*Users*")} -SearchBase "OU=JTEKT North America,DC=nrb,DC=inside" -Properties DistinguishedName -SearchScope Subtree `
| Select DistinguishedName `
| Where {$_.DistinguishedName -notlike "OU=NA Disabled Users,OU=JTEKT North America,DC=nrb,DC=inside" -and `
$_.DistinguishedName -notlike "*Generic*" -and $_.DistinguishedName -notlike "*Administration*" -and `
$_.DistinguishedName -notlike "*TestCompany*" -and $_.DistinguishedName -notlike "*Migrated*"} | Sort-Object DistinguishedName

$saOUs = Get-ADOrganizationalUnit -Filter {(Name -like "*Users*")} -SearchBase "OU=JTEKT South America,DC=nrb,DC=inside" -Properties DistinguishedName -SearchScope Subtree `
| select DistinguishedName | where {$_.distinguishedName -notlike "*Generic*" -and $_.distinguishedName -notlike "*Disable*" -and $_.distinguishedName -notlike "*Terminated*"}

foreach($ou in $OUs){
    Write-Host $ou.DistinguishedName -ForegroundColor Yellow
    (Get-ADUser -SearchBase $ou.DistinguishedName -Filter 'enabled -eq $true' -Properties Name, sAMAccountType, msExchRecipientTypeDetails |`
    #Where user is a standard account > user account that's not a room resource > and not a sharedMBX
    Where-Object{$_.sAMAccountType -eq "805306368" -and $_.msExchRecipientTypeDetails -ne "8589934592" -and $_.msExchRecipientTypeDetails -ne "34359738368"} | select -ExpandProperty Name).count
}
foreach($saou in $saOUs){
    Write-Host $saou.DistinguishedName -ForegroundColor Yellow
    (Get-ADUser -SearchBase $saou.DistinguishedName -Filter 'enabled -eq $true' -Properties Name, sAMAccountType, msExchRecipientTypeDetails |`
    #Where user is a standard account > user account that's not a room resource > and not a sharedMBX
    Where-Object{$_.sAMAccountType -eq "805306368" -and $_.msExchRecipientTypeDetails -ne "8589934592" -and $_.msExchRecipientTypeDetails -ne "34359738368"} | select -ExpandProperty Name).count
}