$SGSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "http://nrbvexch1601.nrb.inside/PowerShell/"
Import-PSSession $SGSession

$DG = Read-Host "Enter the email address of the Distribution Group"
Write-Host "If errors are returned, please check spelling or the group does not have an existing email address" -ForegroundColor Yellow
"`r`n"

try
{
    $DistroGroup = Get-DistributionGroup -Identity $DG -ErrorAction Stop
}
Catch [System.Management.Automation.RuntimeException]
{
    Write-Host "Group not found"
    break
}

$users = Read-Host "Enter the users' email addresses that need to be added to the group. *Separate by comma and no spaces*"
$userSplit = $users.Split(",",20)

foreach($user in $userSplit)
{
    Write-Host $user
    #Add-DistributionGroupMember -Identity $DistroGroup.Name -Member $user
}
